const test = require('ava')
const meaningOfLife = require('../src/someModule')

test('Real meaning of life', (t) => {
  t.is(meaningOfLife(), 42)
})
