const gulp = require('gulp');
const standard = require('gulp-standard');
const ava = require('gulp-ava');

gulp.task('unittest', () => {
    gulp.src('./test/**.js')
        .pipe(ava({ verbose: true }))
}
)

gulp.task('linttest', () => {
    gulp.src('./src/**.js')
        .pipe(standard())
        .pipe(standard.reporter('default', {
            breakOnError: true,
            quiet: false
        }));
})

gulp.task('test', ['linttest','unittest'])